﻿using UnityEngine;
using System.Collections;

public class Instantiation : MonoBehaviour {

	public GameObject gameObj = null;
	// Use this for initialization
	void Start () {

		gameObj = Instantiate(Resources.Load("Prefabs/Cloud")) as GameObject;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
